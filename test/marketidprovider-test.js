const tap = require('tap');
const marketIdProvider = require('../lib/marketidprovider');

tap.test('Rejects failed response', async (t) => { // eslint-disable-line require-await
  const provider = marketIdProvider(() => Promise.reject(new Error('invalid')));
  t.rejects(provider('id'));
});

tap.test('Rejects invalid response', async (t) => { // eslint-disable-line require-await
  const provider = marketIdProvider(() => Promise.resolve('invalid'));
  t.rejects(provider('id'));
});

tap.test('Rejects missing market name', async (t) => { // eslint-disable-line require-await
  const provider = marketIdProvider(() => Promise.resolve([{ id: 1, name: 'first' }, { id: 2, name: 'second' }]));
  t.rejects(provider('testmarket'), new Error('Can not obtain id for market testmarket'));
});

tap.test('Resolves matching market id', async (t) => {
  const provider = marketIdProvider(() => Promise.resolve([{ id: 1, name: 'first' }, { id: 2, name: 'second' }]));
  const id = await provider('second');
  t.equals(id, 2);
});
