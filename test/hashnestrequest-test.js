const tap = require('tap');
const nock = require('nock');
const hashnestRequest = require('../lib/hashnestrequest');

tap.test('Retries 5 times before failing', async (t) => { // eslint-disable-line require-await
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/account\?access_key=KEY&nonce=[0-9]{13}&signature=SIGNATURE$/)
    .times(5)
    .reply(401)
    .post(/^\/api\/v1\/account\?access_key=KEY&nonce=[0-9]{13}&signature=SIGNATURE$/)
    .reply(201, '{"valid":true}', { 'Content-Type': 'application/json' });
  const { request } = hashnestRequest({ sign: () => 'SIGNATURE', key: () => 'KEY' });
  t.rejects(request('/api/v1/account'));
});

tap.test('Can succeed with retries', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/account\?access_key=KEY&nonce=[0-9]{13}&signature=SIGNATURE$/)
    .times(3)
    .reply(401)
    .post(/^\/api\/v1\/account\?access_key=KEY&nonce=[0-9]{13}&signature=SIGNATURE$/)
    .reply(201, '{"valid":true}', { 'Content-Type': 'application/json' });
  const { request } = hashnestRequest({ sign: () => 'SIGNATURE', key: () => 'KEY' });
  const result = await request('/api/v1/account');
  t.same(result, { valid: true });
});

tap.test('Can handle arguments when building path', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/account\?argument=VALUE&access_key=KEY&nonce=[0-9]{13}&signature=SIGNATURE$/)
    .reply(201, '{"valid":true}', { 'Content-Type': 'application/json' });
  const { request } = hashnestRequest({ sign: () => 'SIGNATURE', key: () => 'KEY' });
  const result = await request('/api/v1/account?argument=VALUE');
  t.same(result, { valid: true });
});
