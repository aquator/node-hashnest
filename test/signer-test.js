const tap = require('tap');
const Signer = require('../lib/signer');

tap.test('HMAC-SHA256 Signature', (t) => {
  const signer = new Signer('username', 'access-key', 'secret');
  t.equals(signer.sign(12345), '6f03d0c0f978b798b62153cb099f85a2dc97f702262e50e71a8d4bdc4aa5aa48');
  t.end();
});

tap.test('Key is stored', (t) => {
  const signer = new Signer('username', 'access-key', 'secret');
  t.equals(signer.key(), 'access-key');
  t.end();
});
