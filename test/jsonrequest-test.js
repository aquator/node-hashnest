const tap = require('tap');
const nock = require('nock');
const request = require('../lib/jsonrequest.js');

tap.test('Rejected on error', async (t) => { // eslint-disable-line require-await
  nock('https://www.hashnest.com')
    .post('/api/v1/account')
    .replyWithError('Something went wrong');
  t.rejects(request({
    hostname: 'www.hashnest.com',
    method: 'POST',
    path: '/api/v1/account'
  }), new Error('Something went wrong'));
});

tap.test('Rejects 200 text/html', async (t) => { // eslint-disable-line require-await
  nock('https://www.hashnest.com')
    .post('/api/v1/account')
    .reply(200, '<html/>', { 'Content-Type': 'text/html' });
  t.rejects(request({
    hostname: 'www.hashnest.com',
    method: 'POST',
    path: '/api/v1/account'
  }), new Error('Invalid Content-type: text/html'));
});

tap.test('Rejects invalid application/json', async (t) => { // eslint-disable-line require-await
  nock('https://www.hashnest.com')
    .post('/api/v1/account')
    .reply(200, '{invalid}', { 'Content-Type': 'application/json' });
  t.rejects(request({
    hostname: 'www.hashnest.com',
    method: 'POST',
    path: '/api/v1/account'
  }), new Error('Unexpected token i in JSON at position 1'));
});

tap.test('Accepts valid 200 application/json', async (t) => {
  nock('https://www.hashnest.com')
    .post('/api/v1/account')
    .reply(200, '{"valid":true}', { 'Content-Type': 'application/json' });
  const response = await request({
    hostname: 'www.hashnest.com',
    method: 'POST',
    path: '/api/v1/account'
  });
  t.same(response, { valid: true });
});
