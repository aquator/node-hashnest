/* eslint-disable object-curly-newline */
const tap = require('tap');
const nock = require('nock');
const Hashnest = require('../lib/hashnest');

tap.test('Account info', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/account\?access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '{"id":12,"email":"xxx@bitmain.com","temp_access_token":"xxxxxxxx"}', { 'Content-Type': 'application/json' });
  const hashnest = new Hashnest('username', 'KEY', 'secret');
  const result = await hashnest.getAccountInfo();
  t.same(result, { id: 12, email: 'xxx@bitmain.com', temp_access_token: 'xxxxxxxx' });
});

tap.test('Account balance', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/currency_accounts\?access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '[{"currency": {"code": "btc"},"amount": "41.0449535","blocked": "0.0","total": "41.0449535"},{"currency": {"code": "ltc"},"amount": "0.0","blocked": "0.0","total": "0.0"}]', { 'Content-Type': 'application/json' });
  const hashnest = new Hashnest('username', 'KEY', 'secret');
  const result = await hashnest.getAccountBalance();
  t.same(result, [
    { currency: { code: 'btc' }, amount: '41.0449535', blocked: '0.0', total: '41.0449535' },
    { currency: { code: 'ltc' }, amount: '0.0', blocked: '0.0', total: '0.0' }
  ]);
});

tap.test('Hash rate balance', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/hash_accounts\?access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '[{"currency": {"code": "AntS1"},"amount": "0.0","blocked": "0.0","total": "0.0"},{"currency": {"code": "AntS2"},"amount": "980.0","blocked": "0.0","total": "980.0"}]', { 'Content-Type': 'application/json' });
  const hashnest = new Hashnest('username', 'KEY', 'secret');
  const result = await hashnest.getHashRateBalance();
  t.same(result, [
    { currency: { code: 'AntS1' }, amount: '0.0', blocked: '0.0', total: '0.0' },
    { currency: { code: 'AntS2' }, amount: '980.0', blocked: '0.0', total: '980.0' }
  ]);
});

tap.test('Active orders with market id', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/orders\/active\?currency_market_id=1&access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '[{"id":48544,"category":"sale","amount":"1000.0","ppc":"0.01","created_at":"2014-12-09 01:00:25"}]', { 'Content-Type': 'application/json' });
  const hashnest = new Hashnest('username', 'KEY', 'secret');
  const result = await hashnest.getActiveOrders(1);
  t.same(result, [{ id: 48544, category: 'sale', amount: '1000.0', ppc: '0.01', created_at: '2014-12-09 01:00:25' }]);
});

tap.test('Active orders with market name', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/currency_markets\?access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '[{"id":10,"name":"ANTS1/BTC"},{"id":11,"name":"ANTS2/BTC"}]', { 'Content-Type': 'application/json' })
    .post(/^\/api\/v1\/orders\/active\?currency_market_id=11&access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '[{"id":48544,"category":"sale","amount":"1000.0","ppc":"0.01","created_at":"2014-12-09 01:00:25"}]', { 'Content-Type': 'application/json' });
  const hashnest = new Hashnest('username', 'KEY', 'secret');
  const result = await hashnest.getActiveOrders('ANTS2/BTC');
  t.same(result, [{ id: 48544, category: 'sale', amount: '1000.0', ppc: '0.01', created_at: '2014-12-09 01:00:25' }]);
});

tap.test('Order history with market id', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/orders\/history\?currency_market_id=1&access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '[{"ppc":"0.00001","amount":"4.0","total_price":"0.00004","created_at":"2014-12-09 01:06:00"}]', { 'Content-Type': 'application/json' });
  const hashnest = new Hashnest('username', 'KEY', 'secret');
  const result = await hashnest.getOrderHistory(1);
  t.same(result, [{ ppc: '0.00001', amount: '4.0', total_price: '0.00004', created_at: '2014-12-09 01:06:00' }]);
});

tap.test('Order history with market name', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/currency_markets\?access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '[{"id":10,"name":"ANTS1/BTC"},{"id":11,"name":"ANTS2/BTC"}]', { 'Content-Type': 'application/json' })
    .post(/^\/api\/v1\/orders\/history\?currency_market_id=10&access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '[{"ppc":"0.00001","amount":"4.0","total_price":"0.00004","created_at":"2014-12-09 01:06:00"}]', { 'Content-Type': 'application/json' });
  const hashnest = new Hashnest('username', 'KEY', 'secret');
  const result = await hashnest.getOrderHistory('ANTS1/BTC');
  t.same(result, [{ ppc: '0.00001', amount: '4.0', total_price: '0.00004', created_at: '2014-12-09 01:06:00' }]);
});

tap.test('Create sell order with market id', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/orders\?currency_market_id=1&amount=1000&ppc=0\.01&category=sale&access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '{"id":48544,"category":"sale","amount":"1000.0","ppc":"0.01","created_at":"2014-12-09 01:00:25"}', { 'Content-Type': 'application/json' });
  const hashnest = new Hashnest('username', 'KEY', 'secret');
  const result = await hashnest.createSellOrder(1, 1000, 0.01);
  t.same(result, { id: 48544, category: 'sale', amount: '1000.0', ppc: '0.01', created_at: '2014-12-09 01:00:25' });
});

tap.test('Create sell order with market name', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/currency_markets\?access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '[{"id":10,"name":"ANTS1/BTC"},{"id":11,"name":"ANTS2/BTC"}]', { 'Content-Type': 'application/json' })
    .post(/^\/api\/v1\/orders\?currency_market_id=11&amount=1000&ppc=0\.01&category=sale&access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '{"id":48544,"category":"sale","amount":"1000.0","ppc":"0.01","created_at":"2014-12-09 01:00:25"}', { 'Content-Type': 'application/json' });
  const hashnest = new Hashnest('username', 'KEY', 'secret');
  const result = await hashnest.createSellOrder('ANTS2/BTC', 1000, 0.01);
  t.same(result, { id: 48544, category: 'sale', amount: '1000.0', ppc: '0.01', created_at: '2014-12-09 01:00:25' });
});

tap.test('Create purchase order with market id', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/orders\?currency_market_id=1&amount=1000&ppc=0\.01&category=sale&access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '{"id":48544,"category":"sale","amount":"1000.0","ppc":"0.01","created_at":"2014-12-09 01:00:25"}', { 'Content-Type': 'application/json' });
  const hashnest = new Hashnest('username', 'KEY', 'secret');
  const result = await hashnest.createSellOrder(1, 1000, 0.01);
  t.same(result, { id: 48544, category: 'sale', amount: '1000.0', ppc: '0.01', created_at: '2014-12-09 01:00:25' });
});

tap.test('Create purchase order with market name', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/currency_markets\?access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '[{"id":10,"name":"ANTS1/BTC"},{"id":11,"name":"ANTS2/BTC"}]', { 'Content-Type': 'application/json' })
    .post(/^\/api\/v1\/orders\?currency_market_id=11&amount=1000&ppc=0\.01&category=purchase&access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '{"id":48544,"category":"purchase","amount":"1000.0","ppc":"0.01","created_at":"2014-12-09 01:00:25"}', { 'Content-Type': 'application/json' });
  const hashnest = new Hashnest('username', 'KEY', 'secret');
  const result = await hashnest.createPurchaseOrder('ANTS2/BTC', 1000, 0.01);
  t.same(result, { id: 48544, category: 'purchase', amount: '1000.0', ppc: '0.01', created_at: '2014-12-09 01:00:25' });
});

tap.test('Cancel order', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/orders\/revoke\?order_id=12345&access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '{"success":true}', { 'Content-Type': 'application/json' });
  const hashnest = new Hashnest('username', 'KEY', 'secret');
  const result = await hashnest.cancelOrder(12345);
  t.same(result, { success: true });
});

tap.test('Cancel all orders with market id', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/orders\/quick_revoke\?currency_market_id=1&access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '{"success":true}', { 'Content-Type': 'application/json' });
  const hashnest = new Hashnest('username', 'KEY', 'secret');
  const result = await hashnest.cancelAllOrders(1);
  t.same(result, { success: true });
});

tap.test('Cancel all orders with market name', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/currency_markets\?access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '[{"id":10,"name":"ANTS1/BTC"},{"id":11,"name":"ANTS2/BTC"}]', { 'Content-Type': 'application/json' })
    .post(/^\/api\/v1\/orders\/quick_revoke\?currency_market_id=10&access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '{"success":true}', { 'Content-Type': 'application/json' });
  const hashnest = new Hashnest('username', 'KEY', 'secret');
  const result = await hashnest.cancelAllOrders('ANTS1/BTC');
  t.same(result, { success: true });
});

tap.test('Get markets', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/currency_markets\?access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '[{"id":10,"name":"ANTS1/BTC"},{"id":11,"name":"ANTS2/BTC"}]', { 'Content-Type': 'application/json' });
  const hashnest = new Hashnest('username', 'KEY', 'secret');
  const result = await hashnest.getMarkets();
  t.same(result, [{ id: 10, name: 'ANTS1/BTC' }, { id: 11, name: 'ANTS2/BTC' }]);
});

tap.test('Get market history with market id', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/currency_markets\/order_history\?currency_market_id=1&access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '[{"ppc": "0.00001","amount": "4.0","total_price": "0.00004","created_at": "2014-12-09 01:06:00"}]', { 'Content-Type': 'application/json' });
  const hashnest = new Hashnest('username', 'KEY', 'secret');
  const result = await hashnest.getMarketHistory(1);
  t.same(result, [{ ppc: '0.00001', amount: '4.0', total_price: '0.00004', created_at: '2014-12-09 01:06:00' }]);
});

tap.test('Get market history with market name', async (t) => {
  nock('https://www.hashnest.com')
    .post(/^\/api\/v1\/currency_markets\?access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '[{"id":10,"name":"ANTS1/BTC"},{"id":11,"name":"ANTS2/BTC"}]', { 'Content-Type': 'application/json' })
    .post(/^\/api\/v1\/currency_markets\/order_history\?currency_market_id=10&access_key=KEY&nonce=[0-9]{13}&signature=[0-9a-f]{64}$/)
    .reply(201, '[{"ppc": "0.00001","amount": "4.0","total_price": "0.00004","created_at": "2014-12-09 01:06:00"}]', { 'Content-Type': 'application/json' });
  const hashnest = new Hashnest('username', 'KEY', 'secret');
  const result = await hashnest.getMarketHistory('ANTS1/BTC');
  t.same(result, [{ ppc: '0.00001', amount: '4.0', total_price: '0.00004', created_at: '2014-12-09 01:06:00' }]);
});
