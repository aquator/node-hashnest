module.exports = function MarketIdProvider(hashnestRequest) {
  return name => getMarketId(hashnestRequest, name);
};

async function getMarketId(request, name) {
  const markets = await request('/api/v1/currency_markets');
  const market = markets.filter(m => m.name === name);
  if (market.length !== 1) {
    throw new Error(`Can not obtain id for market ${name}`);
  }
  return market[0].id;
}
