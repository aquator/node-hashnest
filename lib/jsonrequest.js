const https = require('https');

module.exports = function request(opts) {
  return new Promise((resolve, reject) => {
    https.request(opts, (res) => {
      if (res.headers['content-type'] !== 'application/json') {
        res.resume();
        reject(new Error(`Invalid Content-type: ${res.headers['content-type']}`));
      }
      parse(res)
        .then(result => resolve(result))
        .catch(error => reject(error));
    }).on('error', e => reject(e))
      .end();
  });
};

function parse(response) {
  return new Promise((resolve, reject) => {
    let rawData = '';
    response.setEncoding('utf8');
    response.on('data', (chunk) => { rawData += chunk; });
    response.on('end', () => {
      try {
        const parsedData = JSON.parse(rawData);
        resolve(parsedData);
      } catch (e) {
        reject(e);
      }
    });
  });
}
