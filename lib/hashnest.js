const Signer = require('./signer');
const HashnestRequest = require('./hashnestrequest');
const MarketIdProvider = require('./marketidprovider');

module.exports = function Hashnest(username, key, secret) {
  const signer = new Signer(username, key, secret);
  const { request } = new HashnestRequest(signer);
  const marketIdProvider = new MarketIdProvider(request);
  const getMarketId = m => (Number.isInteger(m) ? Promise.resolve(m) : marketIdProvider(m));
  return {
    getAccountInfo: () => request('/api/v1/account'),
    getAccountBalance: () => request('/api/v1/currency_accounts'),
    getHashRateBalance: () => request('/api/v1/hash_accounts'),
    getActiveOrders: async (market) => {
      const marketId = await getMarketId(market);
      return request(`/api/v1/orders/active?currency_market_id=${marketId}`);
    },
    getOrderHistory: async (market) => { // TODO: page, per page
      const marketId = await getMarketId(market);
      return request(`/api/v1/orders/history?currency_market_id=${marketId}`);
    },
    createSellOrder: async (market, amount, ppc) => {
      const marketId = await getMarketId(market);
      return request(`/api/v1/orders?currency_market_id=${marketId}&amount=${amount}&ppc=${ppc}&category=sale`);
    },
    createPurchaseOrder: async (market, amount, ppc) => {
      const marketId = await getMarketId(market);
      return request(`/api/v1/orders?currency_market_id=${marketId}&amount=${amount}&ppc=${ppc}&category=purchase`);
    },
    cancelOrder: orderId => request(`/api/v1/orders/revoke?order_id=${orderId}`),
    cancelAllOrders: async (market) => {
      const marketId = await getMarketId(market);
      return request(`/api/v1/orders/quick_revoke?currency_market_id=${marketId}`);
    },
    getMarkets: () => request('/api/v1/currency_markets'),
    getMarketHistory: async (market) => {
      const marketId = await getMarketId(market);
      return request(`/api/v1/currency_markets/order_history?currency_market_id=${marketId}`);
    }
  };
};
