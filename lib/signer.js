const crypto = require('crypto');

module.exports = function Signer(username, key, secret) {
  return {
    sign: (nonce) => {
      const hmac = crypto.createHmac('sha256', secret);
      hmac.update(`${nonce}${username}${key}`);
      return hmac.digest('hex');
    },
    key: () => key
  };
};
