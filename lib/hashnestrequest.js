const request = require('./jsonrequest');

module.exports = function HashnestRequest(signer) {
  return {
    request: path => tryRequest(signer, path)
  };
};

function tryRequest(signer, path, retries = 4) {
  const options = createRequestOptions(signer, path);
  return new Promise((resolve, reject) => {
    request(options)
      .then(resolve)
      .catch((error) => {
        if (retries > 0) {
          tryRequest(signer, path, retries - 1)
            .then(resolve)
            .catch(reject);
        } else {
          reject(error);
        }
      });
  });
}

function createRequestOptions(signer, path) {
  const nonce = new Date().getTime();
  const signature = signer.sign(nonce);
  const key = signer.key();
  const sep = path.indexOf('?') < 0 ? '?' : '&';
  return {
    hostname: 'www.hashnest.com',
    port: 443,
    method: 'POST',
    path: `${path}${sep}access_key=${key}&nonce=${nonce}&signature=${signature}`
  };
}
