# HashNest [![build status](https://gitlab.com/aquator/node-hashnest/badges/master/build.svg)](https://gitlab.com/aquator/node-hashnest/commits/master) [![coverage report](https://gitlab.com/aquator/node-hashnest/badges/master/coverage.svg)](https://gitlab.com/aquator/node-hashnest/commits/master) [![License: MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://opensource.org/licenses/MIT) [![Known Vulnerabilities](https://snyk.io/test/npm/hashnest/badge.svg)](https://snyk.io/test/npm/hashnest) [![Downloads Counter](https://img.shields.io/npm/dt/hashnest.svg)](https://www.npmjs.com/package/hashnest)
> An async interface of HashNest API for NodeJS

## Installation
This module has only development dependencies.
Use `npm` to add it to your node module.
````bash
$ npm install --save hashnest
````
## Usage
Based on [HashNest API Documentation](https://www.hashnest.com/hashnest_api) you need to create an
API key:
 - At the [Settings](https://www.hashnest.com/settings) page of HashNest create a new API key with appropriate access for the token.
 - Copy the Secret value, as it will be hidden once confirmed
 - Click on the checkmark, you will get an email soon, and activate your key.

The module has automatic retry included, as requests tend to respond with authorization failed without reason. It will attempt to run the request five times before failing.

#### Initialization
````javascript
const Hashnest = require('hashnest');

// You can find (and change) your Username in your Profile
const hashnest = new Hashnest('Username', 'ApiKey', 'ApiSecret');
````
#### Account Info
````javascript
// Query account information
hasnest.getAccountInfo()
  .then(result => console.log(result))
  .catch(error => console.error(error));
````
#### Currency Account API
````javascript
// Check user's account balance
hasnest.getAccountBalance()
  .then(result => console.log(result))
  .catch(error => console.error(error));
````
#### Hash Rate Account API
````javascript
// Check user's hash rate account balance
hasnest.getHashRateBalance()
  .then(result => console.log(result))
  .catch(error => console.error(error));
````
#### Trading Order API
You can use the Market ID or the Market Name. If you use the name, another query will fetch the ID.
````javascript
// Check user's active entrust order
hasnest.getActiveOrders('ANTS9/BTC')
  .then(result => console.log(result))
  .catch(error => console.error(error));

hasnest.getActiveOrders(21)
  .then(result => console.log(result))
  .catch(error => console.error(error));
````
````javascript
// Check user's trading order (order history)
hasnest.getOrderHistory('ANTS9/BTC')
  .then(result => console.log(result))
  .catch(error => console.error(error));

hasnest.getOrderHistory(21)
  .then(result => console.log(result))
  .catch(error => console.error(error));
````
````javascript
// Create an entrust order (sell)
// Arguments are market id or name, amount, ppc
hasnest.createSellOrder('ANTS9/BTC', 100, 0.00005678)
  .then(result => console.log(result))
  .catch(error => console.error(error));

// Create an entrust order (purchase)
// Arguments are market id or name, amount, ppc
hasnest.createPurchaseOrder(21, 100, 0.00005678)
  .then(result => console.log(result))
  .catch(error => console.error(error));
````
````javascript
// Cancel an entrust order
// Argument is the order id
hasnest.cancelOrder(12345)
  .then(result => console.log(result))
  .catch(error => console.error(error));
````
````javascript
// Cancel all entrust orders for a market
hasnest.cancelAllOrders('ANTS9/BTC')
  .then(result => console.log(result))
  .catch(error => console.error(error));

hasnest.cancelAllOrders(21)
  .then(result => console.log(result))
  .catch(error => console.error(error));
````
#### Open Market API
````javascript
// Obtain all opened markets
hasnest.getMarkets()
  .then(result => console.log(result))
  .catch(error => console.error(error));
````
````javascript
// Obtain the trading order list on pointed market (market history)
hasnest.getMarketHistory('ANTS9/BTC')
  .then(result => console.log(result))
  .catch(error => console.error(error));

hasnest.getMarketHistory(21)
  .then(result => console.log(result))
  .catch(error => console.error(error));
````
